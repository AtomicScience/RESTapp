# syntax=docker/dockerfile:1
FROM openjdk:16-alpine
WORKDIR /app

ADD app.jar app.jar

ENV JAVA_OPTS ""
ENTRYPOINT ["java", "-jar", "app.jar"]