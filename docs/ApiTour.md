# API Tour
This is a step-by-step guide that covers all the features of the project.

## Choosing a server
The first thing to do is to choose the server you would like to test the app on - it can be either an instance at `testserver.atomicscience.ru`, already deployed by me, or the instance you've run localy on your `localhost`

![](images/2021-10-09-09-35-01.png)

> **NOTE!**
>
> If you are using `testserver.atomicscience.ru`, switch to SwaggerHub Proxy
>
> If you are using `localhost`, switch Swagger to route requests through your browser:
> 
> ![](images/2021-10-09-09-53-00.png)

## Cleaning a database
There might still be some data from previous tests, so it would be wise to clean the data storage before going on.

To do so, use one of **debug** functions called *resetStorage*.

Just find the **/debug/resetStorage** endpoint, click on *"Try it out"*, and then click *"Execute"*

![](images/2021-10-09-09-38-17.png)

## Fill the database with test users
Now, our database is empty... and useless. The application provides yet another debug function called **populateDatabase**.

Find the **/debug/populateDatabase** endpoint, click on *"Try it out"*, fill in the amount of users you wish to generate *(I recommend to use 10)* and then click *"Execute"*

![](images/2021-10-09-09-43-18.png)

After this request, you will get the list of all the users the database is populated with.

> **NOTE!**
>
> This request fills database with **randomly** generated users

Copy and paste any user from this list - we will use them later.

This is the user I've chosen from my random list - yours will differ:
![](images/2021-10-09-09-47-22.png)

## Authenticate with user
Now that we have a user, we can authenticate with it.

To do so, find an **/auth** endpoint, click on *"Try it out"*, fill in  credentials and then click *"Execute"*

![](images/2021-10-09-09-55-18.png)

> **NOTE!**
>
> From now on, data in screenshots will differ from yours - change the request parameters and bodies according to the user you have saved from the "Fill the database with test users step"

All the generated users' passwords match their logins *(so unsafe!)*

If you have provided the correct credentials, you will get the **200** responce and the JWT token:

![](images/2021-10-09-09-56-31.png)

Copy it and use in the **Authorize** menu:

![](images/2021-10-09-09-57-45.png)

## Get the information about your user
Users can query imformation about themselves.

To do so, find a **/users/user/{id}** GET endpoint, click on *"Try it out"*, fill in your ID and then click *"Execute"*

![](images/2021-10-09-10-22-58.png)

You will get the information about yourself:

![](images/2021-10-09-10-23-40.png)

Now try to run this method on other IDs and get a bunch of **403**s

## Patch your user
Users can also edit information about themselves

To do so, find a **/users/user/{id}** PATCH endpoint, click on *"Try it out"*, set the body as a JSON object containing the fields you wish to edit and then click *"Execute"*.

In this example, we are changing only the *aboutMe* field:

![](images/2021-10-09-10-37-39.png)

The successfull request will return the changed user object:

![](images/2021-10-09-10-39-12.png)

## Patch your user
The PATCH method also enables user to change their login and password.

Let's try it too:

![](images/2021-10-09-10-54-21.png)

Success!

But now, if we try to run any request, we get a **401** error. That's because our JWT tocken was **invalidated**, and now we need to reauthenticate.

## Promote the user with debug function
Our user currently has a USER role, and has a very limited permission scope. But we can promote them to ADMIN with a yet another debug function

To do so, find a **/debug/promoteToAdmin** endpoint, click on *"Try it out"*, provide an ID of the user and then click *"Execute"*

![](images/2021-10-09-11-00-03.png)

## Get a list of users
Administrators can get a list of users.

To do so, find a **/users** GET endpoint, click on *"Try it out"*, provide a user object and then click *"Execute"*

![](images/2021-10-09-11-03-27.png)

This will yield the list of all the users on the system:

![](images/2021-10-09-11-04-49.png)

## Create a user
Administrators can create users.

To do so, find a **/users** POST endpoint, click on *"Try it out"*, provide a user object and then click *"Execute"*

For example, you can add the following user:

```
{
  "login": "MyLogin",
  "firstName": "Newby",
  "lastName": "User",
  "birthday": "2021-10-09",
  "password": "MyPasswordIsVeryStrongIHope",
  "aboutMe": "Hi, I'm a new guy here",
  "address": "REST API, PostgreSQL Server, Users Database"
}
```
![](images/2021-10-09-11-06-52.png)

This will return an ID of the created user
## Search
Administrators can search for users.

To do so, find a **/users/search** endpoint, click on *"Try it out"*, provide a first and/or last names and then click *"Execute"*

The request will return the list of matches - or **404** if nothing was found

![](images/2021-10-09-11-20-04.png)
> **NOTE!**
>
> Search can be conducted on first or last names, or both of them at the same time.